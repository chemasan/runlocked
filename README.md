runlocked
============
Copyright (c) 2018 Jose Manuel Sanchez Madrid.
This file is licensed under MIT license. See file LICENSE for details.

## Overview
runlocked is a tool for safely running commands in a distributed environments where concurrency and race conditions can become an issue.
It runs a command in a with lock to ensure that only one instance of the command is running at the same time.
It uses [Apache Zookeeper](https://zookeeper.apache.org/) as distributed locking mechanism.

## Example:

This places a lock in `/myapp/lock` on the zookeeper server `myzookeeper:2181` and then runs the command `/usr/local/bin/runbackup`.
When the command finishes (successfully or not), the lock is released.

```
runlocked --zookeeper myzookeeper:2181/myapp --path /lock   /usr/local/bin/runbackup
```
